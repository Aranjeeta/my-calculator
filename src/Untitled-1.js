
// import './App.css';
// import { useEffect, useState, useRef } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import { Container, Row, Col } from 'react-bootstrap';

// function App() {

//   // const btnValues = [
//   //   ["C", "+-", "%", "/"],
//   //   [7, 8, 9, "X"],
//   //   [4, 5, 6, "-"],
//   //   [1, 2, 3, "+"],
//   //   [0, ".", "="],
//   // ];

//   const [calc, setCalc] = useState({
//     sign : "",
//     num : 0,
//     res : 0
//   })

//   const numberClick = (event) => {
//     const val = event.target.innerHTML;
//     setCalc({
//       ...calc,
//       num : calc.num === 0 && val === "0" ? "0" : Number(calc.num + val),
//       // if the sign is empty res is 0 else prev res
//       res : !calc.sign ? 0 : calc.res
//     })
//   }
//   const signClick = (event) =>{
//     const value = event.target.innerHTML;
//     setCalc({
//       ...calc,
//       sign : value,
//       // if result is 0(initial) and num is non-0(not initial)
//       res : !calc.res && calc.num  ? calc.num : calc.res,
//       // set num to inital
//       num : 0
//     })
//   }

//   const equalClick = (event) =>{
//     // if sign string is not empty and num is not zero
//     if(calc.sign && calc.num){
//       // function for calculation
//       const math = (a,b,sign) =>{
//         return sign === "+" ? a+b : sign === "-" ? a-b : sign === "*" ? a*b : a/b;
//       }
//       setCalc({
//         ...calc,
//         // current value i.e. b = 0
//         // calaculate and update result and reset the sign and num
//         res : calc.num === "0" && calc.sign === "/" ? "Zero Division" : math(calc.res,calc.num,calc.sign),
//           sign : "",
//           num : 0
//       })
//     }
//   }

//   return (
//     <div className="App">
//       <header className="App-header">


//         <Container className='border w-50 p-3'>

//           <Row>
//             <Col><input name="result" value={calc.num ? calc.num : calc.res} className='calc-input' /></Col>
//             <Col><input name="numip"  value={0} className='calc-input' /></Col>
//           </Row>
//           <Row>
//             <Col><button className="calc-btn" name="clear">C</button></Col>
//             <Col><button className="calc-btn" name="del">DEL</button></Col>
//             <Col><button className="calc-btn" value="per" name="op">%</button></Col>
//             <Col><button className="calc-btn" name="op" onClick={signClick}>/</button></Col>
//           </Row>
//           <Row>
//             <Col><button className="calc-btn" value="7" name="num" onClick={numberClick}>7</button></Col>
//             <Col><button className="calc-btn" value="8" name="num" onClick={numberClick}>8</button></Col>
//             <Col><button className="calc-btn" value="9" name="num" onClick={numberClick}>9</button></Col>
//             <Col><button className="calc-btn" value="mul" name="op" onClick={signClick}>*</button></Col>
//           </Row>
//           <Row>
//             <Col><button className="calc-btn" value="4" name="num" onClick={numberClick}>4</button></Col>
//             <Col><button className="calc-btn" value="5" name="num" onClick={numberClick}>5</button></Col>
//             <Col><button className="calc-btn" value="6" name="num" onClick={numberClick}>6</button></Col>
//             <Col><button className="calc-btn" value="sub" name="op" onClick={signClick}>-</button></Col>
//           </Row>
//           <Row>
//             <Col><button className="calc-btn" value="1" name="num" onClick={numberClick}>1</button></Col>
//             <Col><button className="calc-btn" value="2" name="num" onClick={numberClick}>2</button></Col>
//             <Col><button className="calc-btn" value="3" name="num" onClick={numberClick}>3</button></Col>
//             <Col><button className="calc-btn" value="add" name="op" onClick={signClick}>+</button></Col>
//           </Row>
//           <Row>
//             <Col xs="6"><button className="calc-btn" value={0} name="0">0</button></Col>
//             <Col><button className="calc-btn" value="dec" name="dec">.</button></Col>
//             <Col><button className="calc-btn" value="equal" name="equal" onClick={equalClick}>=</button></Col>
//           </Row>
//         </Container>

//       </header>
//     </div>
//   );
// }

// export default App;
