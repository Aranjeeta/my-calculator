
import './App.css';
import { useEffect, useState, useRef } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Row, Col } from 'reactstrap';
// import { Container, Row, Col } from 'react-bootstrap';

function App() {

  const [history, setHistory] = useState([]);

  const [open, setOpen] = useState(false);

  const btnValues = [
    ["C", "-+", "%", "/"],
    [7, 8, 9, "*"],
    [4, 5, 6, "-"],
    [1, 2, 3, "+"],
    [0, ".", "=", "Hist"],
  ];

  const [calc, setCalc] = useState({
    sign: "",
    num: 0,
    res: 0
  })

  const [historyRec, setHistoryRec] = useState([]);

  const [inp, setInp] = useState("");

  const previousInputValue = useRef("");

  const [eq, setEq] = useState(false);

  const numberClick = (event) => {
    const val = event.target.innerHTML;
    setCalc({
      ...calc,
      num: calc.num === 0 && val === 0 ? 0 : Number(calc.num + val),
      // if the sign is empty res is 0 else prev res
      res: !calc.sign ? 0 : calc.res
    })
    setInput(val);
  }

  const signClick = (event) => {
    const val = event.target.innerHTML;
    setCalc({
      ...calc,
      sign: val,
      // if result is 0(initial) and num is non-0(not initial)
      res: !calc.res && calc.num ? calc.num : calc.res,
      // set num to inital
      num: 0
    })
    setInput(val);
  }

  const equalClick = (event) => {
    // if sign string is not empty and num is not zero
    if (calc.sign && calc.num) {
      // function for calculation
      const math = (a, b, sign) => {
        return sign === "+" ? a + b : sign === "-" ? a - b : sign === "*" ? a * b : a / b;
      }
      setCalc({
        ...calc,
        // current value i.e. b = 0
        // calaculate and update result and reset the sign and num
        res: calc.num === 0 && calc.sign === "/" ? "Zero Division" : math(Number(calc.res), Number(calc.num), calc.sign),
        sign: "",
        num: 0
      })
    }
    previousInputValue.current = inp;
    setInp("");
    setEq(true);
  }

  const perClick = () => {
    let num = calc.num ? parseFloat(calc.num) : 0;
    let res = calc.res ? parseFloat(calc.res) : 0;
    setCalc({
      ...calc,
      num: num /= Math.pow(100, 1),
      res: res /= Math.pow(100, 1),
      sign: ""
    })
  }

  const decClick = (event) => {
    const val = event.target.innerHTML;
    setCalc({
      ...calc,
      num: !calc.num.toString().includes(".") ? calc.num + val : calc.num,
    });
    setInput(val);
  };

  const invertClick = () => {
    setCalc({
      ...calc,
      num: calc.num ? calc.num * -1 : 0,
      // res: calc.res ? calc.res * -1 : 0,

    });

    // replace last digit with negative one
    // setInput(inp.slice(0, -1) + calc.num * -1);
  };

  const resetClick = () => {
    setCalc({
      ...calc,
      num: 0,
      res: 0,
      sign: ""
    })
    setInp("");
  }

  const setInput = (val) => {
    val === "Hist" ? setInp("") : setInp(inp + val);
  }

  const histClick = () => {
    setOpen(true);
    // console.log(history);
  }

  const clearClick = () => {
    setHistory([]);
  }

  const showClick = (event) => {
    const { value } = event.target;
    console.log("value", value.result);
  }

  useEffect(() => {
    // console.log(calc);
  }, [calc])

  useEffect(() => {
    let values = [...historyRec];
    values.push({
      inputVal: previousInputValue.current,
      result: calc.res
    })
    setHistoryRec(values);
  }, [calc.res])

  useEffect(() => {
    if (eq) {
      setHistory([...history, historyRec.at(-1)])
    }
    setEq(false);
  }, [historyRec])

  return (
    <div className="App">
      <header className="App-header">
        {/* <Container className='container-box'>
          <Col> */}
            <div className='button-box'>
            <input name="result" value={calc.num ? calc.num : calc.res} className='calc-input' />
            <input name="numip" value={inp} className='calc-input' />
              {btnValues.flat().map((btn, idx) => {
                return (<button key={idx} className={btn === 0 ? "calc-btn" : "calc-btn"} value={btn} onClick={btn === "C" ?
                  resetClick : btn === "%" ? perClick : btn === "=" ? equalClick :
                    btn === "+" || btn === "-" || btn === "*" || btn === "/" ? signClick : btn === "." ? decClick :
                      btn === "-+" ? invertClick : btn === "Hist" ? histClick :
                        numberClick}>{btn}</button>)
              })}
            </div>
          {/* </Col>
          <Col>
            History Col
          </Col>
        </Container> */}

      </header>

      <div>
        <Modal isOpen={open} >
          <ModalHeader >History</ModalHeader>
          <ModalBody>
            {history.length === 0 ? "Nothing to display" : history.map((ele) => {
              // console.log(idx,ele);
              return (<p>{ele.inputVal}={ele.result} <Button size="sm" value={ele} onClick={showClick} color="secondary" >Show</Button></p>)
            })}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={clearClick}>Clear</Button>
            <Button color="secondary" onClick={() => setOpen(false)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>

    </div>
  );
}

export default App;
